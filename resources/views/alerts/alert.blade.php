@if(session('msg'))
    <div class="alert alert-success" role="alert">

        {{session('msg')}}

    </div>
@endif


@if($errors->any())

    <div class="alert alert-danger" role="alert">
        <strong>ERRoR:</strong>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>

    </div>
@endif


@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading text-center">All Books</div>

            <div class="panel-body">
                        {{$books->links()}}
                        @foreach($books as $book)
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="{{asset('storage/thumbnails/'.$book->image)}}" class="img-responsive"/>
                                </div>
                                <!-- /.col-md-12 -->
                                <div class="col-md-9 text-center">
                                    <h2>{{$book->title}}</h2>
                                    <p>{{$book->info}}</p>
                                    <br/>
                                    Author : {{$book->author}} <br/>
                                </div>
                                <!-- /.col-md-9 -->
                            </div>
                            <hr>
                        @endforeach
            </div>
    </div>
    @endsection
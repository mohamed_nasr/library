<?php $i=1;?>
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Categories</h3>
            <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                <!-- Here is a label for example -->
                <button type="button" class="btn btn-success"><a href="{{route('categories.create')}}">Add Category</a></button>
            </div>

            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Category Name</th>
                </tr>
                </thead>
                <tbody>

                @if(count($categories)>0)
                    @foreach($categories as $category)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$category->name}}</td>
                        </tr>
                    @endforeach
                @endif

                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <!-- box-footer -->
    </div>
    <!-- /.box -->


@endsection




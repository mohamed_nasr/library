<?php $i=1;?>
@extends('adminlte::page')

@section('title', 'Add category')

@section('content')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Add category</h3>
            <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                <!-- Here is a label for example -->
            </div>

            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form action="{{route('categories.store')}}" method="post" autocomplete="off">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="category">Category</label>
                    <input type="text" name="category" class="form-control" id="category" aria-describedby="emailHelp" placeholder="Enter category">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <!-- /.box-body -->

    </div>
    <!-- /.box -->


@endsection

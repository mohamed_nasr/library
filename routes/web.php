<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(
    [
        'prefix'=>'admin',
        'middleware'=>'roles',
        'roles'=>'admin'
    ]
    ,function(){
        Route::resource('users','AdminUserController');
        Route::resource('categories','AdminCategoryController');
        }
);
Route::group(
    [
        'prefix'=>'user',
        'middleware'=>'auth',
    ]
    ,function(){
    Route::get('upload','UserUploadController@index')->name('upload.index');
    Route::post('upload','UserUploadController@upload')->name('upload.save');
    Route::get('categories/{id}','PagesController@viewcategories')->name('viewcategory');

}
);


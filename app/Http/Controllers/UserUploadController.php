<?php

namespace App\Http\Controllers;
use App\LibraryBook;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class UserUploadController extends Controller
{
    public function index()
    {

        return view('admin.userupload');
    }


    public function upload(Request $request){
        $this->validate($request,[
            'title'=>'required',
            'author'=>'required',
            'info'=>'required',
            'image'=>'required|image',
            'book'=>'required|mimes:pdf',
        ]);
        if ($request->hasFile('image')){
            $imageExt = $request->file('image')->getClientOriginalExtension();
            $imageName = time().'thumnnail.'.$imageExt;
            $request->file('image')->storeAs('thumbnails/',$imageName);
        }
        if ($request->hasFile('book')){
            $bookExt = $request->file('book')->getClientOriginalExtension();
            $bookName = time().'book.'.$bookExt;
            $request->file('book')->storeAs('books/',$bookName);
        }
        $book = new LibraryBook();
        $book->title = $request->input('title');
        $book->author = $request->input('author');
        $book->info = $request->input('info');
        $book->image = $imageName;
        $book->bookfile = $bookName;
        $book->user_id = auth()->user()->id;
        $book->category_id = $request->input('category');
        $book->save();
        return redirect(route('upload.index'))->with('msg','Upload OK');
    }
}

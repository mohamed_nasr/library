<?php

namespace App\Http\Controllers;

use App\Category;
use App\LibraryBook;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function viewcategories($id)
    {
        $category=Category::find($id);
        $books=$category->books;
        return view('pages.viewcategory')->with(['books'=>$books,'category'=>$category]);

    }
    public function index()
    {

        $books=LibraryBook::latest()->paginate(3);
        return view('welcome')->with('books',$books);
    }
}
